from ast import parse
import json
from dateutil.parser import parse
from datetime import datetime
import zoneinfo
from constants import YR_LEGENDS


def utc_to_norwegian(date, format=True):
    if format:
        return (
            parse(date)
            .astimezone(zoneinfo.ZoneInfo('Europe/Paris'))
            .strftime('%H:%M')
        )
    return parse(date).astimezone(zoneinfo.ZoneInfo('Europe/Paris'))


def json_to_celsius_send(json_response):
    timeseries = json_response['properties']['timeseries'][0]['data']
    legend = timeseries['next_1_hours']['summary']['symbol_code']
    send_str = f"Tempen på `Ljan` nå er: `{timeseries['instant']['details']['air_temperature']}°C` ({YR_LEGENDS[legend]['desc_nb']})\n\n"
    celsius_list = []
    for i in range(1, 10):
        norweg_time = utc_to_norwegian(
            json_response['properties']['timeseries'][i]['time']
        )
        norweg_datetime = utc_to_norwegian(
            json_response['properties']['timeseries'][i]['time'], format=False
        )
        air_temperature = json_response['properties']['timeseries'][i]['data'][
            'instant'
        ]['details']['air_temperature']
        legend = json_response['properties']['timeseries'][i]['data'][
            'next_1_hours'
        ]['summary']['symbol_code']
        celsius_list.append(
            (norweg_datetime, norweg_time, air_temperature, legend)
        )
    for i in celsius_list:
        humanized_time_hour = int(
            (
                i[0]
                - datetime.now().astimezone(zoneinfo.ZoneInfo('Europe/Paris'))
            ).total_seconds()
        )
        if humanized_time_hour < 0:
            continue
        if (humanized_time_hour / 60 / 60) < 1:
            humanized_time = int(humanized_time_hour / 60)
            send_str += f'Kl: {i[1]} Om {humanized_time} minutter `{i[2]}°C`'
        else:
            send_str += f'Kl: {i[1]} Om {int(humanized_time_hour/60/60)} timer `{i[2]}°C`'
        send_str += f' ({YR_LEGENDS[legend]["desc_nb"]})\n'

    return send_str.strip()


with open('sample_json_compact.json') as f:
    js = json.load(f)

celsius_list = json_to_celsius_send(js)

print(celsius_list)
