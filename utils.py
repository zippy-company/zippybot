from ipaddress import IPv4Address, IPv4Network, AddressValueError
from discord import Embed


def valid_ipv4_and_not_local(ip):
    if not isinstance(ip, str):
        return False
    try:
        check_ip = IPv4Address(ip)
        if check_ip.is_private:
            return False
        return True
    except AddressValueError:
        return False


def ipembed(json_response):
    embed = Embed(
        title=json_response['query'],
        description=json_response['country'],
    )
    embed.add_field(
        name='ISP',
        value=json_response['isp'],
    )
    embed.add_field(
        name='Region',
        value=json_response['region'],
    )
    embed.add_field(
        name='City',
        value=json_response['city'],
    )
    embed.add_field(
        name='Timezone',
        value=json_response['timezone'],
    )
    embed.add_field(
        name='ZIP',
        value=json_response['zip'],
    )
    embed.add_field(
        name='Latitude',
        value=str(json_response['lat']),
    )
    embed.add_field(
        name='Longitude',
        value=str(json_response['lon']),
    )
    org = (
        json_response['org']
        if json_response['org']
        else 'No organization found'
    )
    embed.add_field(
        name='Organization',
        value=org,
    )
    embed.add_field(
        name='AS',
        value=json_response['as'],
    )
    return embed
