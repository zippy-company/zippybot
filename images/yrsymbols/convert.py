import subprocess
from pathlib import Path

svgs = [f for f in Path.cwd().glob('*')]

for f in svgs:
    if f.suffix == '.svg':
        subprocess.call(['inkscape', f.name, f'--export-png={f.stem}.png'])
