import locale
import time
import aiohttp
import subprocess
from datetime import datetime, timedelta
from os import environ

from dotenv import load_dotenv

from discord import Intents, Embed
from discord.ext.commands.errors import MissingRequiredArgument
from discord.ext import commands

try:
    import zoneinfo
except ImportError:
    from backports import zoneinfo

from constants import (
    ADMINS,
    EVO_NAMES,
    LJAN_URL,
    EVO_URL,
    IPAPI_URL,
    MAGNUS_JOBB,
    ZIPPY_HEADER,
    VAKTLISTE_SHIFTS,
)
from utils import valid_ipv4_and_not_local, ipembed
from ljan import json_to_embed

locale.setlocale(locale.LC_TIME, ('nb_NO', 'UTF-8'))

global last_met_call
global json_response
json_response = ''
last_met_call = datetime.now()

load_dotenv()

DISCORD_TOKEN = environ['TOKEN']

intents = Intents.default()
intents.members = True
intents.message_content = True

bot = commands.Bot(command_prefix='?', intents=intents)


@bot.event
async def on_ready():
    print('-' * 10)
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('-' * 10)


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, MissingRequiredArgument):
        await ctx.send(
            '**Du har skrevet en kommando feil!\nDet mangler argumenter til kommandoen.**'
        )
    else:
        await ctx.send(error)


@bot.command()
async def ljan(ctx):
    global last_met_call, json_response
    td = datetime.now() - last_met_call
    get_new_data = td / timedelta(minutes=60) > 1
    if (get_new_data or not json_response):
        async with aiohttp.ClientSession(headers=ZIPPY_HEADER) as session:
            async with session.get(LJAN_URL) as response:
                if response.status == 200:
                    json_response = await response.json()
                    embed = await json_to_embed(json_response)
                    await ctx.send(embed=embed)
                    last_met_call = datetime.now()
    else:
        embed = await json_to_embed(json_response)
        await ctx.send(embed=embed)


@bot.command()
async def run(ctx, *args):
    if ctx.author.id in ADMINS:
        output = subprocess.check_output(
            args,
            universal_newlines=True,
        )

        await ctx.send(output)


@bot.command()
async def evo(ctx, *, senter_to_look_for: str):
    dt_hour = datetime.now(tz=zoneinfo.ZoneInfo('Europe/Paris')).hour
    url = EVO_URL(senter_to_look_for)
    if dt_hour >= 0 and dt_hour < 5:
        await ctx.send(f'**EVO er stengt nå. Kun åpent fra 05-24**')
        return
    if url is not None:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                if response.status == 200:
                    json_response = await response.json()
                    name = json_response['name']
                    max_cap = json_response['max_capacity']
                    current = json_response['current']
                    percentage_usage = json_response['percentageUsed']
                    await ctx.send(
                        f'`{name}`: Det er `{current}` av `{max_cap}` som trener der nå.\nDet er `{percentage_usage:.2f}%` fullt!'
                    )
    else:
        if len(senter_to_look_for) >= 3:
            senter_list = [
                s for s in EVO_NAMES if senter_to_look_for.lower() in s
            ]
            if senter_list:
                await ctx.send(
                    f'Mente du: **{", ".join([s.title() for s in senter_list])}?**'
                )
                return
        await ctx.send(
            f'**{senter_to_look_for} er ikke et gyldig EVO Senter**.\nTilgjengelige sentere er:\n\n{", ".join([n.title() for n in EVO_NAMES])}',
        )


@bot.command()
async def ip(ctx, *, ip: str):
    if valid_ipv4_and_not_local(ip):
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{IPAPI_URL}{ip}') as response:
                if response.status == 200:
                    json_response = await response.json()
                    if json_response['status'] == 'success':
                        await ctx.send(embed=ipembed(json_response))
                    else:
                        await ctx.send(
                            f'**Fail: {json_response["message"].title()}**'
                        )
    else:
        await ctx.send(f'**{ip} is not a valid ip!**')


@bot.command()
async def magnusjobb(ctx):
    async with aiohttp.ClientSession() as session:
        async with session.get(MAGNUS_JOBB) as response:
            if response.status == 200:
                json_response = await response.json()
                date_iso = json_response['date']
                dt = (
                    datetime.fromisoformat(date_iso)
                    .strftime('%A,%e.%b')
                    .title()
                )
                type_of_shift = VAKTLISTE_SHIFTS[json_response['type']]
                depname = json_response['depname']
                await ctx.send(
                    f'Magnus jobber neste gang: `{dt}` kl. `{type_of_shift[1]}`\nAvdeling: `{depname}`'
                )


bot.run(DISCORD_TOKEN)
