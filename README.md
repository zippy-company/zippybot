# zippybot



### Installation

    python -m venv venv

    source venv/bin/activate 

    pip install -r requirements.txt


### Run bot

    # First activate the environment 
    source venv/bin/activate
    
    # Then run in with:
    python bot.py
    OR to run in the background:
    python bot.py &


### To use the blue code formatter

    Just write 'blue .' in the projects root folder

## List of commands

**?ljan** - *Displays the weather about Ljan*
**?evo <senter>** - *Displays the status on an EVO training center. 
