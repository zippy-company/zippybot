from discord import Embed

from dateutil.parser import parse
from datetime import datetime

try:
    import zoneinfo
except ImportError:
    from backports import zoneinfo


from constants import (
    YR_SYMBOL_KEY_MAP,
    YR_LEGENDS,
    LIGHT_GREEN_HEX,
    YR_SYMBOLS_URL,
)


def utc_to_norwegian(date, format=True):
    if format:
        return (
            parse(date)
            .astimezone(zoneinfo.ZoneInfo('Europe/Paris'))
            .strftime('%H:%M')
        )
    return parse(date).astimezone(zoneinfo.ZoneInfo('Europe/Paris'))


async def json_to_embed(json_response):
    weather_symbol = json_response['properties']['timeseries'][1]['data'][
        'next_1_hours'
    ]['summary']['symbol_code']
    png_url = f'{YR_SYMBOLS_URL}{YR_SYMBOL_KEY_MAP[weather_symbol]}.png'
    timeseries = json_response['properties']['timeseries'][0]['data']
    legend = timeseries['next_1_hours']['summary']['symbol_code']
    if '_' in legend:
        legend = legend.split('_')[0]
    title = f'Tempen på `Ljan` nå er: '
    yr_legend = YR_LEGENDS[legend]['desc_nb']
    description = f"`{timeseries['instant']['details']['air_temperature']}°C` ({yr_legend})"

    # CREATE EMBED
    embed = Embed(title=title, description=description, color=LIGHT_GREEN_HEX)
    embed.add_field(name='\u200B', value='\u200B')
    embed.set_thumbnail(url=png_url)

    # Loop through the timeseries
    celsius_list = []
    for i in range(1, 8):
        norweg_time = utc_to_norwegian(
            json_response['properties']['timeseries'][i]['time']
        )
        norweg_datetime = utc_to_norwegian(
            json_response['properties']['timeseries'][i]['time'], format=False
        )
        air_temperature = json_response['properties']['timeseries'][i]['data'][
            'instant'
        ]['details']['air_temperature']
        legend = json_response['properties']['timeseries'][i]['data'][
            'next_1_hours'
        ]['summary']['symbol_code']
        if '_' in legend:
            legend = legend.split('_')[0]

        yr_legend = YR_LEGENDS[legend]['desc_nb']

        celsius_list.append(
            (norweg_datetime, norweg_time, air_temperature, legend)
        )
    for i in celsius_list:
        humanized_time_hour = int(
            (
                i[0]
                - datetime.now().astimezone(zoneinfo.ZoneInfo('Europe/Paris'))
            ).total_seconds()
        )
        if humanized_time_hour < 0:
            continue
        if (humanized_time_hour / 60 / 60) < 1:
            humanized_time = int(humanized_time_hour / 60)
            embed.add_field(
                name=f'Kl: {i[1]} Om {humanized_time} minutter',
                value=f'`{i[2]}°C`({yr_legend})',
                inline=False,
            )
        else:
            embed.add_field(
                name=f'Kl: {i[1]} Om {int(humanized_time_hour/60/60)} timer',
                value=f'`{i[2]}°C`({yr_legend})',
                inline=False,
            )

    return embed
